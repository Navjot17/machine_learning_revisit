Bayes Classification: Used for newsgroup classification
        Dataset: UCI website
        Accuracy: 98.3%
        
Kmeans: For digits recognition
        Dataset: MNIST dataset
        Accuracy: 81.3%
        
These projects are part of assignmentf of Machine Learning course (COL774) by Parag Singla, CSE IIT Delhi