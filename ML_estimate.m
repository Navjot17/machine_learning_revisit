clear
H = load('H.mat');
B = load('B.mat');
F = load('F.mat');
L = load('L.mat');
X = load('X.mat');

H = H.H;
B = B.B;
F = F.F;
L = L.L;
X = X.X;
%parameters of H
H_one = find (H == 1);
H1 = length(H_one)/length(H);
H0 = 1 - H1;
%parameters of B
Bq = abs(B - 1);
Hq = abs(H - 1);
H_zero = find(H == 0);
H_one = find(H == 1);
B0H0 = Bq.*Hq;
B0H0 = sum(B0H0)/length(H_zero);
B1H1 = B.*H;
B1H1 = sum(B1H1)/length(H_one);
B0H1 = Bq.*H;
B0H1 = sum(B0H1)/length(H_one);
B1H0 = B.*Hq;
B1H0 = sum(B1H0) / length(H_zero);
%parameters of L
Lq = abs(L - 1);
Hq = abs(H - 1);
H_zero = find(H == 0);
H_one = find(H == 1);
L0H0 = Lq.*Hq;
L0H0 = sum(L0H0)/length(H_zero);
L1H1 = L.*H;
L1H1 = sum(L1H1)/length(H_one);
L0H1 = Lq.*H;
L0H1 = sum(L0H1)/length(H_one);
L1H0 = L.*Hq;
L1H0 = sum(L1H0) / length(H_zero);
%paramters for X
Xq = abs(X - 1);
Lq = abs(L - 1);
L_zero = find(L == 0);
L_one = find(L == 1);
X0L0 = Xq.*Lq;
X0L0 = sum(X0L0)/length(L_zero);
X1L1 = X.*L;
X1L1 = sum(X1L1)/length(L_one);
X0L1 = Xq.*L;
X0L1 = sum(X0L1)/length(L_one);
X1L0 = X.*Lq;
X1L0 = sum(X1L0) / length(L_zero);
%parameters of F
Fq = abs(F - 1);
F0L0B0 = Fq.*Lq.*Bq;
L0B0 = Lq.*Bq;
F0L0B0 = sum(F0L0B0)/sum(L0B0);
F0L0B1 = Fq.*Lq.*B;
L0B1 = Lq.*B;
F0L0B1 = sum(F0L0B1)/sum(L0B1);
F0L1B0 = Fq.*L.*Bq;
L1B0 = L.*Bq;
F0L1B0 = sum(F0L1B0)/sum(L1B0);
F0L1B1 = Fq.*L.*B;
L1B1 = L.*B;
F0L1B1 = sum(F0L1B1)/sum(L1B1);
F1L0B0 = F.*Lq.*Bq;
L0B0 = Lq.*Bq;
F1L0B0 = sum(F1L0B0)/sum(L0B0);
F1L0B1 = F.*Lq.*B;
L0B1 = Lq.*B;
F1L0B1 = sum(F1L0B1)/sum(L0B1);
F1L1B0 = F.*L.*Bq;
L1B0 = L.*Bq;
F1L1B0 = sum(F1L1B0)/sum(L1B0);
F1L1B1 = F.*L.*B;
L1B1 = L.*B;
F1L1B1 = sum(F1L1B1)/sum(L1B1);

%% log likelihood over test data using the learned parameters
BT = load('BT.mat'); BT = BT.B;
LT = load('LT.mat'); LT = LT.L;
XT = load('XT.mat'); XT = XT.X;
HT = load('HT.mat'); HT = HT.H;
FT = load('FT.mat'); FT = FT.F;
Log_estimate = 0;
for i = 1:2000
    s = 1;
    if (HT(i) == 0) %P(H)
        s = s * H0;
        if (LT(i) == 0) %P(L|H)
            s = s * L0H0;
        else
            s = s * L1H0;
        end
        if (BT(i) == 0) %P(B|H)
            s = s * B0H0;
        else
            s = s * B1H0;
        end
    else 
        s = s * H1;
        if (LT(i) == 0)
            s = s * L0H1;
        else
            s = s * L1H1;
        end
        if (BT(i) == 0)
            s = s * B0H0;
        else
            s = s * B1H0;
        end
    end
    if (XT(i) == 0)
        if (LT(i) == 0)
            s = s * X0L0;
        else
            s = s * X0L1;
        end
    else
        if (LT(i) == 0)
            s = s * X1L0;
        else
            s = s * X1L1;
        end        
    end
    Log_estimate = Log_estimate + log(s); %-2.336e+03
end