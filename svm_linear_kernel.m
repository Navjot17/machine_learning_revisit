clear 
clc

m = 2500;
n = 1558;
C = 1;
train_data = fopen('train.dat', 'r');
test_data = fopen('test.dat', 'r');
outid = fopen('exp1.txt', 'w');
outid2 = fopen('exp2.txt', 'w');

t_data = fileread('train.dat');
modified_t1 = strrep(t_data, 'nonad', '-1');
train_dt = strrep(modified_t1, 'ad', '1');
fprintf(outid, '%s\n', train_dt);
train_mt = dlmread('exp1.txt');

tt_data = fileread('test.dat');
modified_tt1 = strrep(tt_data, 'nonad', '-1');
train_dtt = strrep(modified_tt1, 'ad', '1');
fprintf(outid2, '%s\n', train_dtt);
test_mt = dlmread('exp2.txt');

y = train_mt(:,1559);
x = train_mt(:,1:1558);

mt = size(test_mt,1);
yt = test_mt(:,1559);
xt = test_mt(:,1:1558);

y2 = repmat(y,1,n);
pro = y2.*x;
Q = pro*pro';
b = ones(m,1);

% cvx_begin
%     variables alfa(m);
%     minimize (0.5*(alfa.'*Q*alfa) - alfa.'*b);
%     subject to 
%         alfa >= 0;
%         alfa <= C;
%         alfa.'*y == 0;
% cvx_end
 
alfa = importdata('alpha_lin_q1.mat');

alpha_count = 0;
for i = 1:m
    if (alfa(i) > 0.0001)
        alpha_count = alpha_count + 1;
    end
end        

 w = zeros(1,n);
 for i = 1:m
      if (alfa(i) > 0.0001)
         wi = alfa(i)*y(i);
         wi = wi*x(i,:);
         w = w + wi;
      end
 end
%calculating b
max = -1/0;
min = 1/0;
 for i = 1:m
    if (alfa(i) > 0.0001)
        crnt = w*x(i,:).';
        k = y(i,1);
        if (y(i,1) == 1)
            if (min > crnt)
                min = crnt;
            end
        else if (max < crnt)
             max = crnt;
            end
        end
    end
 end
 
 b = -0.5*(min + max);
% b = -50.2

outputy = zeros(mt,1);
for i = 1:mt
    margin = (w*xt(i,:).' + b); %actualy margin is y times what i have written)
    if (margin >= 0.9999)
        outputy(i,1) = 1;
    end
    if (margin <= -0.9999)
        outputy(i,1) = -1;
    end
end

accuracy = 1 - mae(outputy - yt);
% %accuracy = 0.957

% type 'exp.txt'
