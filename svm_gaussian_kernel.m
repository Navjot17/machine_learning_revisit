clear 
clc

m = 2500;
n = 1558;
C = 1;
train_data = fopen('train.dat', 'r');
test_data = fopen('test.dat', 'r');
outid = fopen('exp1.txt', 'w');
outid2 = fopen('exp2.txt', 'w');

t_data = fileread('train.dat');
modified_t1 = strrep(t_data, 'nonad', '-1');
train_dt = strrep(modified_t1, 'ad', '1');
fprintf(outid, '%s\n', train_dt);
train_mt = dlmread('exp1.txt');

tt_data = fileread('test.dat');
modified_tt1 = strrep(tt_data, 'nonad', '-1');
train_dtt = strrep(modified_tt1, 'ad', '1');
fprintf(outid2, '%s\n', train_dtt);
test_mt = dlmread('exp2.txt');

y = train_mt(:,1559);
x = train_mt(:,1:1558);

yt = test_mt(:,1559);
xt = test_mt(:,1:1558);

% Qmatrix = zeros(2500,2500);
% kernel = zeros(2500,2500);
  gamma = 2.5 * 0.0001;
% for i = 1:m
%     xi = x(i,:);
%     for j = 1:m
%         xj = x(j,:);
%         kernel(i,j) = exp(-0.5*gamma*(norm(xi - xj)^2));
%         Qmatrix(i,j) = y(i,1)*kernel(i,j)*y(j,1);
%     end
% end
%type 'exp1.txt'

 kernel = importdata('kernel_gau_q1.mat');
 Qmatrix = importdata('Qmatrix_gau_q1.mat');
 Q_matrix = Qmatrix.Qmatrix;
 
b = ones(m,1);
% cvx_begin
%     variables alfa_gau(m);
%     minimize (0.5*(alfa_gau.' * Q_matrix * alfa_gau) - alfa_gau.' * b);
%     subject to 
%         alfa_gau >= 0;
%         alfa_gau <= C;
%         alfa_gau.'*y == 0;
% cvx_end

alfa_gau = importdata('alpha_gau_q1.mat');

alpha_count = 0;
for i = 1:m
    if (alfa_gau(i) > 0.0001)
        alpha_count = alpha_count + 1;
    end
end 
%% evaluating b value
% alfa = 0;
% i = 0;
% while (alfa == 0)
%     i = i+1;
%     if ((alfa_gau(i,1)>0) & (alfa_gau(i,1)<C))
%         alfa = alfa_gau(i,1)
%     end
% end

count = 0;
wx = 0;
i = 0;
while(count == 0)
    i = i + 1;
   % if ((alfa_gau(i,1) > 0.0001) & (alfa_gau(i,1) < C-0.0001))
        for j = 1:m
            wx = wx + alfa_gau(j)*y(i,1)*kernel(i,j);
        end
        b = y(i,1) - wx;
        count = 1;
   % end
end

outputy = zeros(779,1);
for i = 1:779
    wxj = 0;
    for j = 1:m
         %  if ((alfa_gau(i,1) > 0.0001) & (alfa_gau(i,1) < C-0.0001))
            xi = xt(i,:);
            xj = x(j,:);
            kernelij = exp(-0.5*gamma*(norm(xi - xj)^2));
            wxj = wxj + alfa_gau(j)*y(j,1)*kernelij;
          % end
    end
    margin = (wxj + b); %actualy margin is y times what i have written)
    if (margin >= 1)
        outputy(i,1) = 1;
    end
    if (margin <= -1)
        outputy(i,1) = -1;
    end
end

accuracy_gau = 1 - mae(outputy - yt); %accuracy = 1

% Optimal value (cvx_optval): -514.384
