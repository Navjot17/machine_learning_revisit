clc
clear all

xfileID = fopen('q4x.dat','r');
yfileID = fopen('q4y.dat','r');

outID = fopen('exp.txt', 'w');
formatSpec = '%f %f'; 
x = fscanf(xfileID, '%f %f', [2 Inf]);
x = x.';
yy = textscan(yfileID, '%s');
yy = yy{:,1};
j = cell2mat(yy);
m = length(yy);
n = 2;
y  = zeros(100,1);
%assuming 1 for alaska
for i = 1:m
    if (j(i,1) == 'C')
        y(i,1) = 0;
    else y(i,1) = 1;
    end;
end

%% 4(a) reporting mean0, mean1 and covariance matrix
sum0 = zeros(1,2);
j0 = 0;
for i = 1:m
    if (y(i,1) == 0)
        sum0 = sum0 + x(i,:);
        j0 = j0 + 1;
    end
end
mean0 = (1/j0)*sum0;
fprintf(outID, 'mean0:\t [%f %f]\n',mean0);

sum1 = zeros(1,2);
j1 = 0;
for i = 1:m
    if (y(i,1) == 1)
        sum1 = sum1 + x(i,:);
        j1 = j1 + 1;
    end
end
mean1 = (1/j1)*sum1;
fprintf(outID, 'mean1:\t [%f %f]\n',mean1);

var = zeros(m,2,2);
for i = 1:m
    if (y(i,1) == 1)
        vv = x(i,:).' - mean1.';
        vv = vv*vv.';
        var(i,:,:) = vv;
    else if (y(i,1) == 0)
            vv = x(i,:).' - mean0.';
            vv = vv*vv.';
            var(i,:,:) = vv;
        end 
    end
end
cov_matrix = zeros(2,2);
cov_mat = (1/m)*sum(var(:,:,:));
for i = 1:2
    for j = 1:2
        cov_matrix(i,j) = cov_mat(1,i,j);
    end
end
fprintf(outID, 'cov_matrix:\n\t %f\t%f \n \t %f\t%f\n',cov_matrix);

%% (4b) plotting training data 
one = find(y == 1); 
zero = find(y == 0);
xplot = plot(x(one, 1), x(one,2), '+');
hold on
yplot = plot(x(zero, 1), x(zero, 2),'o','MarkerFaceColor','red');
title('Training data corresponding to the two input features');
xlabel('growth ring diameters in fresh water');
ylabel('growth ring diameters in marine water');
legend([xplot, yplot],'Alaska', 'Canada');
hold on
%%for p(y=1)
py1 = length(one)/m;
%%for p(y=0)
py0 = length(zero)/m;

%% (4c) deriving decision boundary of the form b'x + c = 0 and plotting
b = (mean1 - mean0)*2*inv(cov_matrix);
c = mean0*inv(cov_matrix)*mean0' - mean1*inv(cov_matrix)*mean1' - log((1-py1)/py1);
x1 = [ min(x(:,1))- 2   max(x(:,1))+2 ];
x2 = -(b(1)*x1 + c)/b(2);
plot(x1,x2, '-', 'Color',[0,1,0], 'LineWidth', 1.5);

%% (4d) Reporting mean0, mean1 and covariance matrices for quadratic decision boundary
fprintf(outID, 'mean0:\t [%f %f]\n',mean0);
fprintf(outID, 'mean1:\t [%f %f]\n',mean1);
var = zeros(m,2,2);
for i = 1:m
    if (y(i,1) == 0)
        vv = x(i,:).' - mean0.';
        vv = vv*vv.';
        var(i,:,:) = vv;
    else if (y(i,1) == 1)
            var(i,:,:) = 0;
        end 
    end
end
cov_matrix0 = zeros(2,2);
m0 = length(zero);
cov_mat0 = (1/m0)*sum(var(:,:,:));
for i = 1:2
    for j = 1:2
        cov_matrix0(i,j) = cov_mat0(1,i,j);
    end
end
fprintf(outID, 'cov_matrix0:\n\t %f\t%f \n \t %f\t%f\n',cov_matrix0);

var1 = zeros(m,2,2);
for i = 1:m
    if (y(i,1) == 1)
        vv = x(i,:).' - mean1.';
        vv = vv*vv.';
        var1(i,:,:) = vv;
    else if (y(i,1) == 0)
            var1(i,:,:) = 0;
        end 
    end
end
cov_matrix1 = zeros(2,2);
m1 = length(one);
cov_mat1 = (1/m1)*sum(var1(:,:,:));
for i = 1:2
    for j = 1:2
        cov_matrix1(i,j) = cov_mat1(1,i,j);
    end
end
fprintf(outID, 'cov_matrix1:\n\t %f\t%f \n \t %f\t%f\n',cov_matrix1);

%% (4e) deriving decision boundary for different convariance matrices case
inv1 = inv(cov_matrix1);
inv0 = inv(cov_matrix0);
phi = py1;
A = inv1- inv0;
B = (mean1*inv1 - mean0*inv0)*2;
C = ( mean0*inv0*mean0' - mean1*inv1*mean1') - 2*log( ((1-phi)*sqrt( det(cov_matrix1)))/(phi*(sqrt( det(cov_matrix0)))));
x1 = [ min(x(:,1)) max(x(:,1)) ];
x2 = [ min(x(:,2)) max(x(:,2))];
fh = @(x1,x2) A(1,1)*x1*x1 + A(2,2)*x2*x2 + ( A(1,2) + A(2,1))*x1*x2 - B(1,1)*x1 - B(1,2)*x2 - C ;
ezplot(fh , x1 , x2);
title('Guassian Discriminant Analysis vs Training data features');
type 'exp.txt';
