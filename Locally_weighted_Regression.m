clc
clear all

xfileID = fopen('q3x.dat','r');
yfileID = fopen('q3y.dat','r');

outID = fopen('exp.txt', 'w');

x = fscanf(xfileID, '%f');
y = fscanf(yfileID, '%f');
m = length(y);

x = [ones(length(x), 1) x];
xx = x(:,2);
%% (2a) using normal equations to get hypothesis function for unweighted linear regression
theta = inv((x.'*x))*(x.'*y);
h = theta(1) + theta(2).*xx;
fprintf('theta unweighted %f %f\n', theta);
%figure
%scatter(xx,y,'filled');
%hold on;
%plot(xx, h, 'Color',[0,1,0]);
title('unweighted linear regression using normal equations')
xlabel('x values')
ylabel('Hypothesis function / raw data')



%% (2b) weighted using analytical solution to find value of theta inverse(X'WX) * (X'WY)
tau = 0.8;
h = zeros(m,1);
for i = 1:m;
    dd = - ((x(i,2) - xx).^2);
    weights = exp(dd./(2*tau^2));          
    w = diag(weights);                                          
    theta = ((x'*w*x)^-1)*(x'*w*y);                         
    h(i) = x(i,:)*theta;                    %predicted value for point x(i)
end
figure
scatter(xx,y,'filled');
hold on;
plot (xx,h,'.', 'Color', 'red', 'MarkerSize', 20);
fprintf('theta weighted %f %f\n', theta);
title('Weighted linear regression using normal equations')
xlabel('x values')
ylabel('Hypothesis function / raw data')


%% (2c) Experimenting for different values of tau
taus = [0.1,0.3,2,10,1000000];
for j = 1:length(taus)
    tau = taus(j);
    h = zeros(m,1);
    for i = 1:m
    dd = - ((x(i,2) - xx).^2);
    weights = exp(dd./(2*tau^2));          
    w = diag(weights);                                          
    theta = ((x'*w*x)^-1)*(x'*w*y);                         
    h(i) = x(i,:)*theta;                    %predicted value for point x(i)
    end
    
    str = sprintf('Weighted linear regression using normal equations with tau = %f',tau);
    figure
    scatter(xx,y,'filled');
    hold on;
    plot(xx, h, '.', 'Color','red', 'MarkerSize', 20);
    title(str)
    xlabel('x values')
    ylabel('Hypothesis function / raw data')
end






