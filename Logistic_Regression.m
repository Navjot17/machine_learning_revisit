clc
clear all

xfileID = fopen('q2x.dat','r');
yfileID = fopen('q2y.dat','r');

outID = fopen('exp.txt', 'w');
formatSpec = '%f %f'; 
x = fscanf(xfileID, '%f %f', [2 Inf]);
x = x.';
y = fscanf(yfileID, '%f');
m = length(y);
x = [ones(length(x), 1) x];

%% (3a) Implementing Newton's method
t = zeros(3,1);  
t0 = t(1);
t1 = t(2);
t2 = t(3);
%matrices to store grad and hessian values for jth training element
grads = zeros(3,m);
hessians = zeros(3,3,m);
for i = 1:10
    for j = 1:m
        xj = x(j,:).';
        kk = t.'*xj;
        h = 1 + exp(-1*kk);
        h = 1/h;
        grad_j = (h - y(j)).*xj;
        hessian_j = h*(1 - h)*(xj*xj.');
        grads(:,j) = grad_j;
        hessians(:,:,j) = hessian_j;
    end
    grad = sum(grads,2)/m;
    hessian = sum(hessians,3)/m;
    t = t - inv(hessian)*grad;
end
fprintf(outID, 'theta obtained: %f %f %f\n',t);

%% (3b) plotting training data and decision boundary fit
yy = x(:,2);
zz = x(:,3);
x2 = -1 * (t(1) + t(2).*yy) / t(3);
figure
plot(yy,x2);
hold on;

one = find(y == 1); 
zero = find(y == 0);

xplot = plot(x(one, 2), x(one,3), '+'); 
hold on
yplot = plot(x(zero, 2), x(zero, 3),'o','MarkerFaceColor','red');
legend([xplot, yplot],'y = 1','y = 0')
xlabel('x1 attribute')
ylabel('x2 attribute')

type('exp.txt');


