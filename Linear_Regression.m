clc
clear all

xfileID = fopen('q1x.dat','r');
yfileID = fopen('q1y.dat','r');

outID = fopen('exp.txt', 'w');

x = fscanf(xfileID, '%f');
y = fscanf(yfileID, '%f');
m = length(y);

%% normalizing
meanx = sum(x(:))/m;
meany = sum(y(:))/m;
x = x - meanx;
y = y - meany;

%% (1a) Linear regression model using gradient descent
t0 = 0; 
t1 = 0;
error = t0 + t1.*x - y;
sq_error = error.*error;
J = sum(sq_error(:))/(2*m);
learn_rate = 0.023;          %%J begins to increase for values above and overshoot at 0.024
it = 1000;                   %%becomes almost constant for values above
t0t1j = zeros(it,3);
Jprev = 100000;
i = 1;
%% declare convergence if J value decreases by less than 0.000001
while (Jprev - J > 0.000001)
    Jprev = J;
    error = t0 + t1.*x - y;
    dj0 = sum(error(:))/m;
    dj10 = error.*x;
    dj1 = sum(dj10(:))/m;
    t0 = t0 - learn_rate*dj0;
    t1 = t1 - learn_rate*dj1;
    error = t0 + t1.*x - y;
    sq_error = error.*error;
    J = sum(sq_error(:))/(2*m);
  % fprintf(outID, '%f\n',J);
    t0t1j(i,1) = t0;
    t0t1j(i,2) = t1;
    t0t1j(i,3) = J;
    i = i+1;
end
fprintf(outID, 'theta0 obtained: %f\n',t0);
fprintf(outID, 'theta1 obtained: %f\n',t1);
%% (1b) plotting raw data vs linear equation obtained
i = i-1;
kk = i;
JX = t0t1j(1:i,3);
figure
plot(1:i, JX);
title('cost J wrt increase in no. of iterations');
xlabel('Number of iterations')
ylabel('Cost J')
h = t0 + t1.*x;
figure
scatter(x,y,'filled');
hold on;
plot(h,x,'Color','red', 'LineWidth', 1.5);
xlabel('x values')
ylabel('Hypothesis function / raw data')

%% (1c) plotting surface for error value and marking points iterated through
J_vals = zeros(100, 100);   
t0_vals = linspace(-10, 4, 100);
t1_vals = linspace(-3, 3, 100);
for i = 1:length(t0_vals)
	  for j = 1:length(t1_vals)
	  t = [t0_vals(i); t1_vals(j)];
      error = t0_vals(i) + t1_vals(j).*x - y;
      sq_error = error.*error;
      J = sum(sq_error(:))/(2*m);
	  J_vals(i,j) = J ;
    end
end
J_vals = J_vals';
figure;
surf(t0_vals, t1_vals, J_vals)
xlabel('theta_0'); ylabel('theta_1')
hold on;
x1 = t0t1j(1:kk,1);
x2 = t0t1j(1:kk,2);
Jz = t0t1j(1:kk,3);
for i = 1:kk
    para1 = x1(i);
    para2 = x2(i);
    error_value = Jz(i);
    plot3(para1,para2,error_value,'ro', 'MarkerFaceColor','red');
    view(103,26);
    hold on;
    pause(0.2);
end
hold off;

%% (1d) building contours
figure
contour(t0_vals, t1_vals, J_vals, logspace(-2, 2, 20))
xlabel('theta_0'); ylabel('theta_1')
title('contour obtained using learning rate = 0.023')
hold on;
for i = 1:kk
    para1 = x1(i);
    para2 = x2(i);
    plot(para1,para2,'ro','MarkerFaceColor','red');
    hold on;
    pause(0.2);
end

%% (1e) Experimenting for different learning rate values
it = 50;
l_values = [0.1,0.5,0.9,1.3,2.1,2.5];
for j = 1:length(l_values)
    learn_rate = l_values(j);
    t0 = 0;
    t1 = 0;
    for i = 1:it
        error = t0 + t1.*x - y;
        dj0 = sum(error(:))/m;
        dj10 = error.*x;
        dj1 = sum(dj10(:))/m;
        t0 = t0 - learn_rate*dj0;
        t1 = t1 - learn_rate*dj1;
        error = t0 + t1.*x - y;
        sq_error = error.*error;
        J = sum(sq_error(:))/(2*m);
 %   fprintf(outID, '%f\n',J);
        t0t1j(i,1) = t0;
        t0t1j(i,2) = t1;
        t0t1j(i,3) = J;
    end
    x1 = t0t1j(1:it,1);
    x2 = t0t1j(1:it,2);
    figure
    contour(t0_vals, t1_vals, J_vals, logspace(-2, 2, 20))
    str = sprintf('Contour for learning rate = %f',learn_rate);
    xlabel('theta_0'); ylabel('theta_1')
    title(str)
    hold on;
    for i = 1:kk
        para1 = x1(i);
        para2 = x2(i);
        plot(para1,para2,'ro','MarkerFaceColor','red');
        hold on;
        pause(0.2);
    end
 %{
    JX = t0t1j(:,3);
    figure
    plot(1:it, JX);
    title('cost J wrt increase in no. of iterations');
    xlabel('Number of iterations')
    ylabel('Cost J')
  %}
    
end
%% output file
type 'exp.txt'



